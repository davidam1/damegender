# Damegender

Gender Detection Tool from the name written in nodejs
That is a minimalistic version of Damegender written in Python and Bash (see: https://github.com/davidam)
+ No Machine Learning
+ No Dependencies
+ Only an api rest client to the json names

You can download the json names with:
$ git clone https://github.com/davidam/damegender-web

## Getting started

$ npm install damegender
$ node index.js David

## License
GPLv3 or later

