// Copyright (C) 2022 David Arroyo Menendez

// Author: David Arroyo Menendez <darroyome@MacBook-Pro-de-David.local> 
// Maintainer: David Arroyo Menendez <darroyome@MacBook-Pro-de-David.local> 
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the 'Damegender'
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom
// the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

const https = require('https');

function guess(firstname) {
    
    mypath = '/names/inter/' + firstname + '_all.json';
    const options = {
	hostname: 'damegender.davidam.com',
	port: 443,
	path: mypath,
	method: 'GET',
    };

    const req = https.request(options, res => {
	console.log(`statusCode: ${res.statusCode}`);
	if (res.statusCode == 200) {
	    res.on('data', d => {
		process.stdout.write(d);
	    });
	}
	if (res.statusCode == 404) {
	    console.log("It doesnt't seem a name");
	}
    });

    req.on('error', error => {
	console.error(error);
    });

    req.end();
    
}

module.exports = { guess };
