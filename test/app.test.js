const https = require('https');
const app = require('../app.js')

string = 'statusCode: 200\n[{\n"name": "DAVID",\n"frequency": 4969643,\n"males": "99.73847215987143 %",\n"females": "0.2615278401285565 %"\n}]';
substring = 'statusCode: 200\n[{\n"name": "DAVID",\n"';

it('renders correctly I', async () => {
    await expect(
	async () => await app.guess("DAVID")
    );
});

it('renders correctly II', async () => {
    await expect(
	async () => await app.guess("JUAN CARLOS")
    ).resolves.toBe.toString(string);
});

it('renders correctly III', async () => {
    await expect(
	async () => await app.guess("MARÍA ANTONIETA")
    ).resolves.toContain.toString(substring);
});

