// Copyright (C) 2022 David Arroyo Menendez

// Author: David Arroyo Menendez <darroyome@MacBook-Pro-de-David.local> 
// Maintainer: David Arroyo Menendez <darroyome@MacBook-Pro-de-David.local> 
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the 'Software'
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom
// the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

// That is a minimalistic version of Damegender written python and bash
// No Machine Learning
// No Dependencies
// Only an api rest client to the json names
// You can download the json names with:
// $ git clone https://github.com/davidam/damegender-web

const app = require('./app.js');

const https = require('https');

const myArgv = process.argv;

if (myArgv.length == 2) {
    console.log('Introduce arguments, please');
    process.exit(1);
}
    
myArgs = myArgv.slice(2);

let text = "";

n = 0;
for (var n = 0; n < myArgs.length; n++) {
    if ( n == 0) {
	text += myArgs[n].toUpperCase();
    } else {
	text += "_" + myArgs[n].toUpperCase();
    }

}

app.guess(text);
